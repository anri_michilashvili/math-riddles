package com.example.mathriddles
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_levels.*
import kotlinx.android.synthetic.main.activity_question.*


class QuestionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
        init()
    }
    fun init() {
        //sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)
        //Function.sharedPreferences
        // val editor = sharedPreferences.edit()
        val qnummer = intent.getIntExtra("questionnum", 0)

        if (qnummer == 1) {
            questiontext.setText("what is 5x5?")
            hinttext.setText("multiplication")
            var answer:String
            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "25") {

                    //answer = answertext.text.toString()
                    //Function.instance!!.saveString(answer,"25")

                    // editor.putString("answer",answer1)
                    // editor.apply()
                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, LevelsActivity::class.java))
                    level2.isClickable = true
                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                }
            }
            if (qnummer == 2) {
                questiontext.setText("what is bla?")
                hinttext.setText("multiplication")
            }
        }
    }
}
