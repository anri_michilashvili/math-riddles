package com.example.mathriddles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_levels.*
import kotlinx.android.synthetic.main.activity_question.*

class LevelsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_levels)
        init()
    }

    //intent.putExtra("Username","John Doe")
    // startActivity(intent)
    fun init() {
        level2.isClickable = false
        level3.isClickable = false
        level4.isClickable = false
        level5.isClickable = false
        level6.isClickable = false
        level7.isClickable = false
        level8.isClickable = false
        level9.isClickable = false

        val intent = Intent(this@LevelsActivity, QuestionActivity::class.java)
        level1.setOnClickListener {
            intent.putExtra("questionnum", 1)
            startActivity(intent)
            //val ans=Function.instance!!.readString("answer")
            //if(ans == "25")
            //{
              //  Toast.makeText(applicationContext, "gamovida", Toast.LENGTH_SHORT).show()
                //level2.isEnabled=true
            //} //else { Toast.makeText(applicationContext, "no", Toast.LENGTH_SHORT).show()}


        }
        //  val click = i.getBooleanExtra("btnclick",false)
        //  level2.setEnabled(click)

        level2.setOnClickListener{
        intent.putExtra("questionnum", 2)
        startActivity(intent)
        }
    }
}


