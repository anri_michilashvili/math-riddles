package com.example.mathriddles
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity

public class Function
{
    companion object{
        var instance:Function?=null
         lateinit var sharedPreferences: SharedPreferences
         lateinit var editor: SharedPreferences.Editor }

    fun init(context: Context) {
        instance = this
        sharedPreferences = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        editor = sharedPreferences!!.edit()
    }

   public fun saveString(key:String,value:String){
        editor.putString(key, value)
        editor.apply()
    }
    public fun removeValue(key: String){
        editor.remove(key)
        editor.apply()
    }
    public fun readString(key: String):String{
        return sharedPreferences!!.getString(key,"")!!
    }
}

